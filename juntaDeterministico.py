import os,glob,sys
import shutil
import subprocess
from subprocess import Popen, PIPE, STDOUT

def juntaDeterministico(filename):
    #filename = sys.argv[1]
    #subprocess.call("tar -zxf "+filename,shell=True)
    outFile = open(filename.split(".")[0]+".fitness", "w")
    outFile.write ("filename\tfitness\n")
    outStatus = open(filename.split(".")[0]+".mipstatus", "w")
    outStatus.write ("filename\tmipstatus\n")
    outTime = open(filename.split(".")[0]+".time", "w")
    outTime.write ("filename\ttime\n")
    for file in glob.glob("./running/output/*"):
        ifile = open(file, "r")
        linha = ifile.readline()
        if (len(linha.strip()) > 0):
            outStatus.write (file.split("/")[-1].split(".")[0] + "\t" + linha.split()[0]+"\n")
            outFile.write (file.split("/")[-1].split(".")[0] + "\t" + linha.split()[1]+"\n")
            outTime.write (file.split("/")[-1].split(".")[0] + "\t" + linha.split()[2]+"\n")
        else:
            print ("Warning: empty file " + file)
        ifile.close()
    outFile.close()
    outStatus.close()
    outTime.close()
    