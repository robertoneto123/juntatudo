import os,glob,sys
import shutil
import subprocess
from subprocess import Popen, PIPE, STDOUT

def juntaParametrizavel(filename):
    outFile = open(filename.split(".")[0]+".fitness", "w")
    outTime = open(filename.split(".")[0]+".time", "w")

    #Para cada nome de arquivo
    primeiroDir = glob.glob("./running/output/*")[0]
    print ("Nomes de arquivos de "+primeiroDir)
    dirs = [a.split("/")[-1] for a in glob.glob("./running/output/*")]
    outFile.write("filename")
    outTime.write("filename")
    for d in dirs:
        outFile.write("\t"+d)
        outTime.write("\t"+d)
    outFile.write("\n")
    outTime.write("\n")

    for arquivos in glob.glob(primeiroDir+"/*"):
        filename = arquivos.split("/")[-1]
        outFile.write(filename)
        outTime.write(filename)
        for d in dirs:
            f = open("./running/output/"+d+"/"+filename)
            linha = f.readline()
            outFile.write("\t"+linha.split()[0])
            outTime.write("\t"+linha.split()[-1])
        outFile.write("\n")
        outTime.write("\n")
    outFile.close()
    outTime.close()