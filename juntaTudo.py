import sys, subprocess, os, glob
import juntaDeterministico
import juntaParametrizavel

for f in sys.argv[1:]:
    #determina o tipo do arquivo
    print ("Processing... "+f)
    subprocess.call("tar -zxf "+f,shell=True)
    if not os.path.isdir(glob.glob("./running/output/*")[0]):
        print (">"+glob.glob("./running/output/*")[0])
        juntaDeterministico.juntaDeterministico(f)
    else:
        juntaParametrizavel.juntaParametrizavel(f)
    subprocess.call("rm -rf running",shell=True)